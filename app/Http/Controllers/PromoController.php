<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request\Facades\Schema;

class PromoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	return view('promo.index');
    }
}
