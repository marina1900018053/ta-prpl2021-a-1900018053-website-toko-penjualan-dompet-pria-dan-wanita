@extends('layouts.app')

@section('content')

    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                <img src="{{ url('images/wallet3.jpeg') }}" class="rounded mx-auto d-block" width="1100">
            </div>
        </div>
    </div>

    <section class="section-testimonial" id="testimonial" >
        <h2 class="text-center mt-5">TESTIMONIAL</h2>
            <div class="owl-carousel row text-center p-5">
               
                <div class="col">
                    <div class="card">
                    <img src="images/user_testimonial_1.png" alt="" class="w-50 mx-auto mt-5">
                        <div class="card-body">
                           <h5 class="card-title">Mickel Jeans</h5>
                           <p class="card-text">
                            " Suka bangettt sama dompett inii hehee. 
                            Walaupun pengemasannya lama tapi worth it lah. 
                            Box nya aga sedikit penyok tapi gapapa ttp bagus. "
                            </p>
                        </div>
                    </div>
                 </div>
                <div class="col">
                    <div class="card">
                    <img src="images/user_testimonial_2.png" alt="" class="w-50 mx-auto mt-5">
                        <div class="card-body">
                        <h5 class="card-title">Annie Hundson</h5>
                         <p class="card-text">
                         " The beautiful wallet for my precious boyfie. 
                         Hahaha I opened the gift before he does cause 
                         I wanna check it first and wow guys it's amazing. 
                         Hope he likes it like I do "
                         </p>
                         </div>
                     </div>
                </div>
                <div class="col">
                    <div class="card">
                    <img src="images/user_testimonial_3.png" alt="" class="w-50 mx-auto mt-5">
                        <div class="card-body">
                        <h5 class="card-title">Grock Melasho</h5>
                        <p class="card-text">
                        " Alhamdulillah dompetnya bagus , 
                        ada kotaknya bisa gampang kalau mau ngasih 
                        hadiah ke orang lain. Seneng deh, super duper
                        worth it bangettt "
                        </p>
                     </div>
                </div>
                </div>
                <div class="col">
                    <div class="card">
                    <img src="images/user_testimonial_6.png" alt="" class="w-50 mx-auto mt-5">
                        <div class="card-body">
                        <h5 class="card-title">Liliana Shaheer </h5>
                        <p class="card-text">
                        " Beli dompet buat kado ultah suami 
                        yg masih seminggu lagi, alhamdulillah dpt harga diskon, 
                        gpp deh kecepetan kasih kado nya.. hehehehhehe "
                        </p>
                    </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                    <img src="images/user_testimonial_5.png" alt="" class="w-50 mx-auto mt-5">
                        <div class="card-body">
                        <h5 class="card-title">Kuva Sonnie Rez</h5>
                        <p class="card-text">
                        " Barang sangat bagusss, bahannya tebel dan warnanya juga baguss. 
                        Sayangnya karena terlalu tebel jadinya susah ditekuk😂 
                        tapi worth to buy "
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer class="section-footer border-top mt-5 mb-1">
        <div class="container pt-5 pb-5">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg-3">
                            <h5>FEATURES</h5>
                            <ul class="list-unstyled">
                                <li><a href="#">Reviews</a></li>
                                <li><a href="#">Community</a></li>
                                <li><a href="#">Social Media Kit</a></li>
                                <li><a href="#">Affliate</a></li>
                            </ul>
                        </div>
                        <div class="col-12 col-lg-3">
                            <h5>ACCOUNT</h5>
                            <ul class="list-unstyled">
                                <li><a href="#">Refund</a></li>
                                <li><a href="#">Security</a></li>
                                <li><a href="#">Rewards</a></li>
                            </ul>
                        </div>
                        <div class="col-12 col-lg-3">
                            <h5>COMPANY</h5>
                            <ul class="list-unstyled">
                                <li><a href="#">Career</a></li>
                                <li><a href="#">Help Center</a></li>
                                <li><a href="#">Media</a></li>
                            </ul>
                        </div>
                        <div class="col-10 col-lg-3">
                            <h5>GET CONNECTED</h5>
                            <ul class="list-unstyled">
                                <li><a href="#">Yogyakarta</a></li>
                                <li><a href="#">Indonesia</a></li>
                                <li><a href="#">0877 - 1911 - 0891</a></li>
                                <li><a href="#">support@MyWallts.com</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid pb-2">
            <div class="row justify-content-center align-items-center border-top pt-1">
                <div class="col-auto text-gray-500 font-weight-light">
                    2021 Copyright Marina Indah Prasasti • All right reserved • Made in Yogyakarta
                </div>
            </div>
        </div>
      </footer>
@endsection


