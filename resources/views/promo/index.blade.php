@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<h4><i class="fa fa-shopping-cart"></i> PROMO</h4>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
				<table class="table">
				<tbody>
						<tr>
							<td width="10" align="center">
								<h3><b>Promo Bulan Juni-Agustus </h3>
								MyWallts lagi ngadain promo yang menarik nih! Mau tau? Ini dia promonya
							</td>
						</tr>
						<tr>
							<td width="10">
							<h4>Discount untuk Pengguna Baru</h4>
							</td>
						</tr>
						<tr>
							<td width="10">Selamat datang di website MyWallts! Kamu bisa mendapatkan potongan 5% pada setiap pembelian produk tanpa 
							minimal transaksi pada Official Website MyWallts, karena kamu sebagai pengguna baru, kamu bisa coba sekarang juga :)
							Selamat Berbelanja di Toko Kami :)</td>
						</tr>
						<tr>
							<td width="10">
							<h4>Discount Setiap Hari Jum'at Wage/Suro</h4>
							</td>
						</tr>
						<tr>
							<td width="10">Kamu bisa mendapatkan potongan 10% pada setiap pembelian produk pada hari Jum'at Wage/Suro dengan 
							minimal transaksi Rp. 500.000,- pada Official Website MyWallts, kamu bisa coba sekarang juga :)
							Selamat Berbelanja di Toko Kami :)</td>
						</tr>
					</tbody>
					</table>
				</div>
			</div>
		</div>
   </div>
</div>

@endsection
